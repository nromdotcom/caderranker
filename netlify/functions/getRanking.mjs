import { getBlobStore, ok, clientError, notFound } from './lib/util.mjs'

/**
 * Fetch a saved ranking from Blobs
 * @param {import('@netlify/functions').HandlerEvent} event 
 * @param {import('@netlify/functions').HandlerContext} ctx 
 * @returns {import('@netlify/functions').HandlerResponse}
 */
const handler = async function (event, ctx) {
  const rankingId = event.queryStringParameters.id
  console.log(`getting ranking ${rankingId}`)
  if (!rankingId || !rankingId.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/)) {
    return clientError({ message: 'invalid or missing ranking id' })
  }

  const store = getBlobStore(event, 'ranks')

  const rank = await store.get(rankingId, { type: 'json' })
  if (!rank) {
    return notFound({ message: 'no ranking found with id' })
  }

  return ok(rank)
}
  
export { handler }