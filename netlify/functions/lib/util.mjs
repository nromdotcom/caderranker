import { getStore } from '@netlify/blobs'

/**
 * Get a Blob store using within a Lambda-compatible function
 * that doesn't have it autoconfigured.
 * 
 * Note: @netlify/blobs claims to have a convenience function for this
 *       https://github.com/netlify/blobs/tree/main#lambda-compatibility-mode
 *       But it's not currently exported by the package so isn't actually
 *       available to use, so we'll do it like this for now.
 * 
 * @param {import('@netlify/functions').HandlerEvent} event Lambda-compatible event object
 * @returns {import('@netlify/blobs').Store} Blob store
 */
function getBlobStore (event, storeName) {
  const rawData = Buffer.from(event.blobs, 'base64')
  const data = JSON.parse(rawData.toString('ascii'))
  
  return getStore({
    deployID: event.headers['x-nf-deploy-id'],
    edgeURL: data.url,
    siteID: event.headers['x-nf-site-id'],
    token: data.token,
    name: storeName
  })
}

/**
 * Pull information about the caller out of the Lambda-compatible context
 * Unauthenticated users get a partial user object with no sub and anonymous provider.
 * @param {import('@netlify/functions').HandlerContext} ctx Lambda-compatible context
 * @returns {NetlifyUser} Netlify user object
 */
function getUser (ctx) {
  const anonymousUser = {
    exp: 0,
    app_metadata: {
      provider: 'anonymous'
    },
    user_metadata: {
      full_name: 'Anonymous User'
    }
  }

  return (!('clientContext' in ctx) || !('user' in ctx.clientContext))
    ? anonymousUser
    : ctx.clientContext.user
}

/**
 * Generate a Lambda-compatible handler response
 * @param {Number} status HTTP status code
 * @param {Object} body JSON object for response body
 * @param {Object} headers Key-value pairs for response headers
 * @returns {import('@netlify/functions').HandlerResponse}
 */
function response (status, body, headers) {
  return {
    statusCode: status,
    body: JSON.stringify(body),
    headers: headers
  }
}

/**
 * OK response
 * @param {Object} body JSON object for response body
 * @param {Object} headers Key-value pairs for response headers
 * @returns {import('@netlify/functions').HandlerResponse}
 */
function ok (body, headers) {
  return response(200, body, headers)
}

/**
 * Client Error response
 * @param {Object} body JSON object for response body
 * @param {Object} headers Key-value pairs for response headers
 * @returns {import('@netlify/functions').HandlerResponse}
 */
function clientError (body, headers) {
  return response(400, body, headers)
}

/**
 * HTTP 404 response
 * @param {Object} body JSON object for response body
 * @param {Object} headers Key-value pairs for response headers
 * @returns {import('@netlify/functions').HandlerResponse}
 */
function notFound (body, headers) {
  return response(404, body, headers)
}

/**
 * Server Error response
 * @param {Object} body JSON object for response body
 * @param {Object} headers Key-value pairs for response headers
 * @returns {import('@netlify/functions').HandlerResponse}
 */
function serverError (body, headers) {
  return response(500, body, headers)
}

export {
  getBlobStore,
  getUser,
  ok, clientError, serverError, notFound
}