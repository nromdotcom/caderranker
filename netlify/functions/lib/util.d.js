// This file contains miscellaneous types we may need access to
// that don't exist in the @netlify packages for example.

/**
 * @typedef NetlifyUser
 * @property {Nummber} exp Auth expiry timestamp (0 if unauthenticated)
 * @property {String} [sub] User subject GUID (undefined if unauthenticated)
 * @property {String} [email] Registered user email address (undefined if unauthenticated)
 * @property {Object} app_metadata Information about the user's auth method
 * @property {String} app_metadata.provider The provider through with the user authenticated
 * @property {Object} user_metadata Additional user information
 * @property {String} user_metadata.full_name The user's registered name or username
 */