import { getBlobStore, ok, clientError, notFound } from './lib/util.mjs'
import { v4 as uuidv4 } from 'uuid'


/**
 * Fetch a saved ranking from Blobs
 * @param {import('@netlify/functions').HandlerEvent} event 
 * @param {import('@netlify/functions').HandlerContext} ctx 
 * @returns {import('@netlify/functions').HandlerResponse}
 */
const handler = async function (event, ctx) {
  const id = uuidv4()
  const body = JSON.parse(event.body)
  if (!Array.isArray(body) || !body.every(r => typeof r === 'string')) {
    return clientError({ message: 'invalid ranking, must be array of strings' })
  }
  console.log(`saving ranking as ${id}`)

  const ranks = getBlobStore(event, 'ranks')
  await ranks.setJSON(id, body)

  return ok({
    id,
    status: 'success'
  })
}

export { handler }
