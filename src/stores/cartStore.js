import { DateTime } from 'luxon'
import { defineStore } from 'pinia'
import cartData from '../data/carts.json'

/**
 * Returns all carts that have a release date in the past.
 * 
 * It finds the release date via the "Release 29/02/24"-type
 * notes that carts will get when they're added to the evercade
 * site as they're announced.
 * 
 * This way we can stage announced carts without adding them
 * to everyone's default collections. Once they're released
 * they will be added to default collections.
 * @returns {Array<Object>} All released carts
 */
function getAllReleasedCarts () {
  const date = DateTime.now()
  return cartData.filter(c => {
    if (!('note' in c) || !c.note.includes('Release')) {
      return true
    }

    const cartDateStr = c.note.split(' ')[1]
    const cartDate = DateTime.fromFormat(cartDateStr, 'dd/MM/yy')

    return cartDate <= date
  })
}

function loadCollection () {
  const coll = localStorage.getItem('cr-collection')
  if (!coll) return getAllReleasedCarts()
  const collObj = JSON.parse(coll)
  return cartData.filter(c => collObj.includes(c.name))
}

export const useCartStore = defineStore('carts', {
  state: () => ({
    carts: cartData,
    collection: loadCollection()
  }),
  actions: {
    updateCollection (coll) {
      localStorage.setItem('cr-collection', JSON.stringify(coll.map(c => c.name)))
      this.collection = coll
    }
  }
})
