import { defineStore } from 'pinia'

export const useRankingStore = defineStore('ranking', {
  state: () => ({
    rankings: {}
  }),
  actions: {
    async saveRanking (ranking) {
      const resp = await fetch('/.netlify/functions/saveRanking', {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(ranking.map(c => c.name))
      })

      const data = await resp.json()
      return data.id
    },

    async getRanking (id) {
      if (id in this.rankings) {
        return this.rankings[id]
      }

        const resp = await fetch(`/.netlify/functions/getRanking?id=${id}`, {
          method: 'GET',
          headers: {
            'accept': 'application/json'
          }
        })

        if (resp.status !== 200) {
          const err = await resp.text()
          throw new Error(err)
        }
        const data = await resp.json()
        this.rankings[id] = data
        return data
    }
  }
})
