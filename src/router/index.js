import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/ranking',
      name: 'doRanking',
      component: () => import('@/views/RankView.vue')
    },
    {
      path: '/ranking/:rankingId',
      name: 'viewRanking',
      component: () => import('@/views/SavedRankingView.vue')
    },
    {
      path: '/collection',
      name: 'collection',
      component: () => import('@/views/CollectionView.vue')
    },
    {
      path: '/faq',
      name: 'faq',
      component: () => import('@/views/EvercadeFaqView.vue')
    },
    {
      path: '/cadeys',
      name: 'cadeys',
      component: () => import('@/views/CadeysArchiveView.vue')
    }
  ]
})

export default router
