# The Basics

## Q: What is Evercade?

A: Well, gosh. Hopefully if you're here you probably already have some idea. But, just in case:

> Evercade is an ecosystem of gaming devices and physical game cartridges containing collections of legally-licensed games for classic consoles, arcades, and home computers as well as retro-inspired modern indie games.

## Q: Uh, geez, can you break that down for me?

A: Yeah, I guess that's a lot to shove into a single sentence.

There are several devices you can use to play Evercade cartridges:

1. **Evercade Handheld** (discontinued): Also known as the "OG" or the "Ancient Hero." This is the original Evercade hardware from 2020. It will play all current carts, but is discontinued and will not be receiving any new updates. It also only has 2 shoulder buttons, which limits controls for certain games.
2. **[Evercade VS](https://evercade.co.uk/vs/)**: The "home console" version of the Evercade. This hardware supports up to 4 player multiplayer. It can play all all cartridges except the 2 Namco carts (due to licensing restrictions). The VS is in active production and receives regular updates.
3. **[Evercade EXP](https://evercade.co.uk/exp/)**: The handheld successor to the Evercade Handheld. The EXP adds two additional shoulder buttons, a more powerful processor and more internal storage. The EXP offers "tate" (vertical) mode so vertical-oriented games (such as shoote em ups) can use the full screen. The EXP also comes with 18 built-in games from Capcom.
4. **[Super Pocket](https://hypermegatech.com)**: These are not "Evercade devices" properly, but do play Evercade cartridges. These are pared-back devices with fewer features than the proper Evercade devices, but offer a great pocketable experience and come with either Capcom or Taito games built-in.

Evercade cartridges are fully-physical collections of games that come with full-color manuals. Each cartridge is numbered to fully activate your collector neurons. There are 3 colors of Evercade cartridge boxes:

1. **Red**: These are "home console" collections. These will contain games for classic hardware such as the Atari 2600, NES, Mega Drive/Genesis, Playstation, etc. Some of the games are older games, contemporary to their hardware, but some are also newer "homebrew" indie games written for the hardware. More recently, red carts have also been released with "native" Evercade games that don't run in an emulator.
2. **Purple**: These are classic arcade games from a variety of developers and publishers.
3. **Blue**: These are "home computer" collections featuring games for classic home computer platforms such as the Commodore 64 and the Amiga. Like with the Red collections, while many of these games are contemporary, there is an increasing amount of newer "homebrew" games for these platforms available.

## Q: So these are emulation? Is that legal?

A: Yes, most games on Evercade are running in software emulation. There are, however, an increasing number of "native" Evercade games that run directly within Evercade's OS without an emulator.

And, yes, whether emulated or not, all games, emulation software, etc on Evercade are fully-licensed from rights holders and legal.
