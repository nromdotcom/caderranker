import Basics from './01-basics.md'
import Games from './02-games.md'
import Misc from './03-misc.md'
import Support from './04-support.md'

export default [ Basics, Games, Misc, Support ]
