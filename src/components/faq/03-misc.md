# Miscellaneous

## Q: Can I use save states?

A: Yes! Well, for emulated games. Each emulated game has 6 save state slots you can use. Native games such as Cathedral and Duke Nukem 1+2 Remastered do not have save states.

Additionally, emulated games that support "SRAM" cartridge saves or memory card saves can use those.

Native games that support saves will have specific save functionality built in, so please consult those games' manuals for more information. Most just use auto-saves, though.

## Q: Where are my saves stored?

A: All sorts of saves - whether save states, SRAM, or native saves - are stored directly on the cartridge so they can be moved freely between devices.

## Q: My SRAM save, save state, or native save isn't working?(!)

A: There are two common causes of issues with saves. Both boil down to "make sure you exit the game completely back to the Evercade menu before powering off your device."

1. For save states or native saves, make sure the save state has finished writing before powering off your device.
2. For SRAM (cartridge/memory card) saves, make sure you have fully exited the game back to the Evercade menu before powering off your device. Saves are only persisted to the cart when the game is closed.

## Q: My VS is acting weird in some way (powering off randomly, games crashing, etc)?(!)

A: Before doing anything else, make sure you are supplying your VS with sufficient power.

Since the VS doesn't come with a power adapter, most folks just make due with what they have (cell phone adapters, plugging into their TV, etc) and often these won't supply the VS with proper amperage, leading to a variety of weird behaviors.

The VS user guide mentions using an adapter that provides at least 5 volt 1 amp. However, firmware updates and native games will often need more like 2 amps during the most intensive operations.

In short, it's recommended you have a high quality power adapter that supplies 5 volts at (at least) 2 amps.

## Q: How can I make a game for Evercade?

A: You can see information on the [Evercade developer program](https://evercade.co.uk/developer/). The page is a _little_ out of date (as of this writing), but ultimately the important part is that you'll want to get in touch at [developer@evercade.co.uk](mailto:developer@evercade.co.uk) to get in touch with Blaze.

Some of the emulation platforms Evercade supports have modern development tooling to help you make your game (such as [nesmaker](https://www.thenew8bitheroes.com/)). Native games will, at a minimum, need to run on Linux/ARM and of course fit within the processor and memory constraints of all Evercade devices. Some native games use the [Godot engine](https://godotengine.org/), but most use custom engines specifically optimized for Evercade versions.

For more specific information on requirements of both emulated and native games, please contact Blaze as this is very much not an official resource.
