# Getting Help

## Q: I have a question or problem that isn't answered here?

A: Here's a list of places for additional information or support:

1. [The Evercade discord](https://discord.com/invite/bBPbfsK) has a lot of very helpful and knowledgeable folks.
1. The [VS](https://evercade.co.uk/support-exp/) and [EXP](https://evercade.co.uk/support-exp/) support pages on the Evercade site have additional FAQs not addressed here. As well as information on patch notes and PDFs of user guides for more information.
1. You can email [support@evercade.co.uk](mailto:support@evercade.co.uk) to get support directly from Blaze. They're very friendly and helpful, so please don't hesitate to contact them if you have any issues.