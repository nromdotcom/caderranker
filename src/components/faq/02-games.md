# Cartridges and Games

## Q: So what games are available on Evercade?

A: For an official listing of Evercade cartridges, you can see [the Evercade website](https://evercade.co.uk/cartridges/). Note that if you scroll down toward the bottom, you can see a "Full games list" section you can open to see (and search) the full list of games without having to click through every cartridge individually.

You can also learn about the EXP built-in games from [its product page on the Evercade website](https://evercade.co.uk/exp/) and the Super Pocket built-in games from [its product page on the HyperMegaTech website](https://www.hypermegatech.com/taito-edition/).

You can also see an unofficial games list on the [evercadeinfo Games Database](https://evercade.info/evercade-games-database/).

## Q: I heard some cartridges are out of print?

A: Yeah, unfortunately. Since all Evercade cartridges are fully-licensed, sometimes those licenses can expire for a variety of reasons. At that point, no further copies of the cartridge can be produced and it is referred to as "Legacy."

You can see the full list of Legacy cartridges on [the Evercade website](https://evercade.co.uk/cartridges/). When new carts are going to go legacy, Blaze provide ample warning and perform a final printing of the cartridge so folks who want a copy have time of find one.

Historically, Legacy carts have stayed in circulation at or near MSRP for a while, but as more people enter the ecosystem that isn't as much of a guarantee anymore. Famously, Technos Collection 1 has been going for exorbitant prices on EBay.

## Q: What carts should I start with?

A: Well, that's going to vary wildly depending on what sorts of games you're into. You can see [my most recent ranking](/ranking/9ed100e1-3cb2-4850-a46b-6208c0c29d47) if you're curious which carts I'm most into, but this won't necessarily hold true for everyone.

You can always [drop into the Evercade Discord](https://discord.com/invite/bBPbfsK) to ask  folks there what they recommend. Just make sure to mention what sorts of games you like so they can try to tailor their suggestions.

## Q: I heard there are secret games?

A: Yes! EXP and VS owners have access to a few hidden games that can be unlocked with secret codes or button presses. And VS owners have access to a few games that can be unlocked by placing two specific carts into the two slots of the VS at the same time.

For a full list of secrets on [evercadeinfo secrets and easter eggs list](https://evercade.info/vs-secrets-and-easter-eggs/).