/**
 * Perform a merge sort on an array of items. You must pass
 * in a selector to compare each item. This function should
 * return a promise.
 * 
 * Note: This is still tied to Cart objects, as it checks the
 * name of the selection.
 * @param {Array} array An array of elements to merge sort
 * @param {Function} selector A function to call to compare two elements
 * @returns {Promise<Array>}
 */
export default async function mergeSort(array, selector) {
  if (array.length === 1) {
    return array
  }

  const middle = Math.floor(array.length / 2)
  const left = array.slice(0, middle)
  const right = array.slice(middle)
  return await merge(
    await mergeSort(left, selector),
    await mergeSort(right, selector),
    selector
  )
}
  
async function merge(left, right, selector) {
  let result = [];
  let leftIndex = 0;
  let rightIndex = 0;

  while (leftIndex < left.length && rightIndex < right.length) {
    const answer = await selector(left[leftIndex], right[rightIndex])
    const fakeL = answer === left[leftIndex].name ? 1 : 0
    const fakeR = answer === right[rightIndex].name ? 1 : 0
  
    if (fakeL < fakeR) {
      result.push(left[leftIndex])
      leftIndex++
    } else {
      result.push(right[rightIndex])
      rightIndex++
    }
  }
  
  return result.concat(left.slice(leftIndex)).concat(right.slice(rightIndex))
}