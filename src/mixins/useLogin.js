import netlifyIdentity from 'netlify-identity-widget'

function init (loginCb, logoutCb) {
  netlifyIdentity.on('init', loginCb)
  netlifyIdentity.on('login', user => {
    loginCb(user)
    netlifyIdentity.refresh()
  })
  netlifyIdentity.on('logout', logoutCb)
  netlifyIdentity.init()
}

function open () {
  netlifyIdentity.open()
}

function getUser () {
  return netlifyIdentity.currentUser
}

export {
  init, open, getUser
}
