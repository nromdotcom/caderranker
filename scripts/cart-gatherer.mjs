/**
 * This is a script that will start to construct the carts json.
 * The current version underwent a lot of post-processing that hasn't
 * been backported to this script yet (combining into a single file,
 * downloading images, etc, etc) but I wanted to make sure we had at
 * least this part here for the future.
 * 
 * TODO Some games return only "Read More" or something - see if the actual
 * description is maybe the element or two before.
 */
import axios from 'axios'
import { writeFile, readFile, stat } from 'fs/promises'
import * as cheerio from 'cheerio'

async function getPage (url) {
  const urlPath = url.split('/cartridges')[1]
  const fileName = urlPath === '/' ? 'index' : urlPath.replace(/\//g, '')
  const path = `./cache/${fileName}`
  let exists = false

  try {
    await stat(path)
    exists = true
  } catch (_) {
    // we don't have a copy of the cart cached
  }

  if (exists) {
    console.log('returning cache' + path)
    return await readFile(path, 'utf8')
  }

  console.log('fetching' + url)
  const resp = await axios.get(url)
  await writeFile(path, resp.data)
  return resp.data
}

const resp = await getPage('https://evercade.co.uk/cartridges/')
const doc = cheerio.load(resp)
const carts = doc('.cartridge-search-results > .cartridge-item').map(function () {
  const el = doc(this)

  const url = el.attr('href')
  const note = el.find('.cartridge-tag').text().trim()
  const img = el.find('.cartridge-image').attr('data-bg-image').match(/url\((.*)\)/)[1]

  const text = el.text().split('\n').reduce((acc, el) => {
    const trimmed = el.trim()
    if (trimmed !== '') acc.push(trimmed)
    return acc
  }, [])

  return {
    name: text[text.length - 1],
    url,
    img,
    note
  }
}).toArray()
console.log(carts)

const output = []
for (const cart of carts) {
  const cartResp = await getPage(cart.url)
  const cartDoc = cheerio.load(cartResp)
  const cartDesc = cartDoc('.cartridges-video-block > * > .content-column > p')
  cart.desc = cartDesc.text()

  const games = cartDoc('.games-content-container > .game-block').map(function () {
    const el = cartDoc(this)

    const img = el.find('img').attr('data-src')
    const name = el.find('span.game-title').text()
    const body = el.find('p').text().trim()
    const genre = el.find('.genre').text().split(': ')[1].replace('Genre', '')
    const players = el.find('.players').text().split(':')[1].trim().replace('Players', '')

    const desc = body.split('\n')[body.split('\n').length - 1].replace('\\n', '')

    return {
      name, img, desc, genre, players
    }
  }).toArray()
  cart.games = games

  output.push(cart)
}

await writeFile(`./output.json`, JSON.stringify(output, null, 2))
