# CaderRanker

Rank yer Cades.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
netlify dev # we can't use vite directly, as we're
            # using the netlify cdn which the dev
            # server can emulate locally.
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### Deploying

Hooked into Netlify. MRs get deployed to preview environments,
merging to main deploys to production.
